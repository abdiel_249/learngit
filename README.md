About this repository
=====================

This repository will be used for 
**"Learning Git"** in the **_SCESI training research for Companies_** and other tools that will be useful in the day to day of the programmer.

## What is GIT?
Que es Git
Conceptos básicos de Git.
Instalación
Configuración
Primeros pasos
Cómo funciona Git
Git Branch Model
Repositorios Remotos
Conexion SSH en GitLab y GitHub
Errores Frecuentes
Extra - Git Flow



## 1. Install Git on Linux

Before install `Git` you must the update your repositories

```shell
sudo apt update
```
after you can install `git`
```sh
sudo apt install git
```
In orden to see which version of git we can have installed we run the following
```sh
git --version
``` 
## Git Configuration 
Once the installation has successfully completed, the next thing to do is to set up the configuration details of the GitHub user. To do this use the following two commands by replacing `user_name` with your GitHub username and replacing `email_id` with your email-id you used to create your GitHub account.

```sh
git config --global user.name "Your Name"
git config --global user.email "youremail@domain.com"
```

## Generating SSH Keys

To generate SSH keys on Windows 10 (Git Bash), Windows WSL and Linux Mint (based in Ubuntu 18), i followed the instructions in the [Gitlab documentation](https://docs.gitlab.com/ee/ssh/) for generates the SSH Keys, and execute the next commands on commands line:
```shell
ssh-keygen -t ed25519 -C "email@example.com"
```
Or, if you want to use RSA:
```shell
`ssh-keygen -t rsa -b 4096 -C "email@example.com"`
```
The -C flag adds a comment in the key in case you have multiple of them and want to tell which is which. It is optional.

Next, you will be prompted to input a file path to save your SSH key pair to. If you don’t already have an SSH key pair and aren’t generating a deploy key, use the suggested path by pressing Enter. Using the suggested path will normally allow your SSH client to automatically use the SSH key pair with no additional configuration.

If you already have an SSH key pair with the suggested file path, you will need to input a new file path and declare what host this SSH key pair will be used for in your `~/.ssh/config` file.
 
 ### Adding your SSH Key in Gitlab acount

Copy your public SSH key to the clipboard located on `~/.ssh/name-file.pub` 
Add your public SSH key to your GitLab account by:

* Clicking your avatar in the upper right corner and selecting **Settings.**
* Navigating to **SSH Keys** and pasting your **public** key from the clipboard into the **Key** field. If you:
    * Created the key with a comment, this will appear in the **Title** field.
    * Created the key without a comment, give your key an identifiable title like Work Laptop or Home Workstation.
* Click the **Add key** button. 
## Testing that everything is set up correctly
To test whether your SSH key was added correctly, run the following command in your terminal (replacing **gitlab.com** with your GitLab’s instance domain):
```shell
    ssh -T git@gitlab.com
```
To test whether your SSH key was added correctly, run the following command in your terminal (replacing `gitlab.com` with your GitLab’s instance domain).

To retain these settings, you’ll need to save them to a configuration file. For OpenSSH clients this is configured in the `~/.ssh/config` file. In this file you can set up configurations for multiple hosts, like GitLab.com, your own GitLab instance, GitHub, Bitbucket, etc.
entra a [hola][hola]

Below are two example host configurations using their own SSH key:
```sh
# GitLab.com
Host gitlab.com
  Preferredauthentications publickey
  IdentityFile ~/.ssh/gitlab_com_rsa

# Private GitLab instance
Host gitlab.company.com
  Preferredauthentications publickey
  IdentityFile ~/.ssh/example_com_rsa
```


[hola]:[google.com]