# Primeros pasos con Git

> Es ensta ocacion quise resumir un poco de informacion para un conocimiento medio de Git, desde la instalacion en linux, trabajando con Git de una forma mas profesional. y un poco del flujo de trabajo con Git Flow. De antemano disculpas por la ortografia (acentos y la enie) ya que cuento con un teclado en ingles y que pereza cambiarlo hahhaha

En este articulo tocaremos los siguientes puntos:
* Que es Git
* Conceptos básicos de Git.
    * Instalación
    * Configuración
    * Primeros pasos
* Cómo funciona Git
* Git Branch Model
* Repositorios Remotos
* Conexion SSH en GitLab y GitHub
* Errores Frecuentes
* Extra - Git Flow

## Que es Git?
**Git** es un sistema de control de versiones de código abierto ideado por **Linus Torvalds** _(el padre del sistema operativo Linux)_ y actualmente es el sistema de control de versiones más extendido.

El control de versiones es un sistema que registra cambios en un archivo o conjunto de archivos a lo largo del tiempo para poder recuperar versiones específicas más tarde. Para los ejemplos se usará el código fuente del software como los archivos de versión controlada, _(.txt, .java, .js, .html, .css, etc.),_
aunque en realidad puedes hacerlo con casi cualquier tipo de archivo en un ordenador.

## Conceptos basicos de Git
La instalacion de git es bastanate sencilla y daremos los ejemplo en los sistemas GNU/Linux, para los usuarios de Mac OS y Windows pueden descaargar el software desde la pagina oficial de [**Git**](https://git-scm.com/) e instalarlo.
### Instalacion
Para distribuciones basadas en **Debian** puede ejecutar el siguiente comando en la terminal del equipo:

``` shell
    $ sudo apt install git
```
Para distribuciones basadas en **Arch**
``` shell
    $ sudo pacman -S git
```
si utilizas otra distribucion de GNU/Linux solo debes instalarlo con el gestor de paquetes que tengas.
En algunos casos talvez debas actualizar primero la lista de paquetes disponibles y sus versiones.

Comprobamos que se instalo correctamente ejecutando:

``` shell
    $ git --version
    git version 2.xx.x
```
Actualmente yo tengo instalada la version `2.17.1` y es probable que en tu caso sea diferente, es recomendable usar versiones superiores a la `2.xx.x` para no tener problemas en el funcionamineto, pero si recien lo estas instalando no tendras ese problema 

### Configuracion
La configuracion basica necesaria es la de nuestro usuario y correo electronico.
``` shell
    $ git config --global user.name "tu nombre"

    $ git config --global user.email someone@example.com
```
Ojo. _"no coloques las llaves o corchetes `[]`"_
verificamos que se guardaron correctamente con el comando:
``` shell
    $ git config --list
    user.email=someone@example.com
    user.name=tu nombre
    color.ui=auto
```
es posible que veas mas configuraciones pero ahora solo nos centraremos en que `user.name` y `user.email` sean correctos.

### Primeros Pasos
1. Primero debemos **iniciar un repositorio** local 

``` sh
$ git init
```

este comando iniciara el repositorio en el directorio actual y creara `.git/` en el cual estara guardado toda la informacion.  Tambien puede usar el comando `git init [nameFolder]` para crear una repositorio en la carpeta _"nameFolder"_.

2. Consultar el estado con:
``` sh
    $ git status
```
si no hay archivos en el directorio mostrara un resultado asi:

``` sh
    On branch master
    
    No commits yet
    
    nothing to commit (create/copy files and use "git add" to track)
```
si existieran archivos nos mostrara algo asi:

``` sh
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	index.css
	index.html
	test.js

nothing added to commit but untracked files present (use "git add" to track)
```

notaras que en ambos casos lo primero que nos muestra es la rama (`branch`) actual, `No commits yet` nos dice que aun no se han hecho commits, y por ultimo  vera los archivos a los cuales no se le esta haciendo el seguimiento.

3. hacer un `commit` 

Ahora podemos ejecutar:

``` sh
$ git add [fileName]
``` 

 para aniadir un archivo especifico, o 
 
 ``` sh
 $ git add .
 ```
para aniadir todos los archivos. en este punto los archivos se encuentran en el `staging area` que no analizaremos a fondo mas adelante. si volvemos a ejecutar `git status` veremos que  ahora los archivos aparecen bajo el bloque: `Changes to be commited` entoces podemos ejecutar:

``` sh
git commit -m "frist commit"
````

el texto debe describir los cambios que estubimos haciendo hasta el momento, alternativamente podemos usar;

``` sh
git commit 
```

que abrira el editor de texto de consola que tengamos (Vim, Nano u otro). felicidades ya hiciste tu primer commit.

4. Crear nuevas ramas, bifurcasion `branch`

en los pasos anteriores vimos que la rama `master` esta desde el momento en que iniciamos el repositorio  y es porque es la rama principal de todo el proyecto pero podremos ver el uso apropiado en el tema **Git Branch Model**.

Para crear una nueva:

``` sh
 git branch [nameBranch]
```

No nos mostrara nada, a menos que se hallan equivocado en el comando, para el ejemplo crearemos una rama con el nombre `test` asi que debemos ejecutar:

``` sh
    $ git branch
    * master
    test
```

veremos que se nos marca con un `*` (asterisco) la rama en la que estamos actualmente `master` y la rama `test`, ahora debemos movernos a la nueva rama.

5. **Moviendose entre ramas**

``` sh
git checkout [nameBranch]
Switched to branch 'nameBranch'
```
Con ese comando nos cambiaremos a la rama especificada, si tienes un error con el comando o con el nombre de la rama te mostrara en la consola.  para el caso del ejemplo nos moveremos a la rama `test` y al volver a ejecutar `git branch` veremos algo asi:

``` sh
    $ git branch
    master
     * test
```
_las ramas se muestran en orden alfabetico._
Otra forma de **Crear y Moverse** a otra rama con un solo comando es:

``` sh
    $ git checkout -b [newBranch]
    Switched to a new branch 'newBranch'
```
con ello habremos creado una nueva rama y estaremos posicionados en esa rama.

Para **eliminar** una rama:
``` sh
    $ git branch -d [nameBranch]
    Deleted branch [name branch] (was def636e).
```

En el caso de que esa rama contenga trabajos sin fusionar, el comando anterior nos devolverá el siguiente error:

``` sh
error: The branch 'nameBranch' is not an ancestor of your current HEAD.
If you are sure you want to delete it, run 'git branch -D nameBranch'.
```

6. Ver los `commit` que se realizaron hasta el momento:

``` sh
$ git log
commit def636e80b866f720bb08b59f62feec950dcf499 (HEAD -> newBranch)
Author: author <someone@example.com>
Date:   Sun Apr 19 19:01:57 2020 -0400

    frist commit
```


## Como Funciona Git
## Git Branch Model
## Repositorios Remotos
## Conexxion SSH con un Repositorio Remoto
## Errores Frecuentes
## Git FLow